#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "src/tipos.h"
#include "src/funcoes.h"


void main()
{   
    Imagem *imagem;
    char nomeImagem[20];
    char nomeCompleto[50];
    char nomeImagemNova[20];
    char nomeCompletoNovo[50];
    int colunas;
    int linhas;
    int variacaoCor;
    int quantidadePixel;
    char formato[3];
    Pixel *vetorDePixel;
    int resp;
    do
    {
        printf("Digite o numero da opção que deseja escolher:\n" 
                "1 - Aplicar escala de cinza\n" 
                "2 - Aplicar detecção de bordas\n" 
                "3 - Aplicar filtro​ blurring\n" 
                "4 - Aplicar filtro sharpening\n" 
                "5 - Aplicar rotação da imagem\n" 
                "6 - Aplicar ampliação da imagem\n" 
                "7 - Aplicar redução da imagem\n"
                "0 - Para sair\n");
        scanf("%i", &resp);
        switch (resp)
        {
        case 1:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            escalaCinza(nomeCompletoNovo,linhas,colunas,quantidadePixel, vetorDePixel, variacaoCor);
            break;
        case 2:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            detBorda(nomeCompletoNovo, linhas, colunas, quantidadePixel, vetorDePixel, variacaoCor);
            break;
        case 3:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            borrarImagem(nomeCompletoNovo, linhas, colunas, quantidadePixel, vetorDePixel, variacaoCor);
            break;
        case 4: 
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            sharpenImagem(nomeCompletoNovo, linhas, colunas, quantidadePixel, vetorDePixel, variacaoCor);
            break;
        case 5:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            break;
        case 6:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            break;
        case 7:
            caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto,&colunas, &linhas, &vetorDePixel, &variacaoCor, &quantidadePixel, formato);
            break;

        }
        
    } while (resp!=0);
    
}
