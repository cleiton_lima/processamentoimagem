#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tipos.h"

void caminhoImagem(char nomeImagem[], char nomeCompleto[],char nomeImagemNova[],char nomeCompletoNovo[]){
    printf("Digite o nome da imagem que deseja ler:");
    scanf("%s", nomeImagem);
    strcpy(nomeCompleto, "imagens/"); 
    strcat(nomeCompleto, nomeImagem); 
    strcat(nomeCompleto, ".ppm");
    printf("Digite o nome da imagem que deseja imprimir:");
    scanf("%s", nomeImagemNova);
    strcpy(nomeCompletoNovo, "imagens/"); 
    strcat(nomeCompletoNovo, nomeImagemNova); 
    strcat(nomeCompletoNovo, ".ppm");    
    
    
}

void lerImagem(char nomeCompleto[],int *colunas, int *linhas, Pixel **vetorDePixel, int *variacaoCor, int *quantidadePixel, char formato[]){
       FILE *file;
       file = fopen(nomeCompleto, "r");
    if (file == NULL)
    {
        printf("\n O arquivo não foi lido! \n");
        exit(0);
    }
        fscanf(file, "%s", formato);
        fscanf(file, "%i %i", colunas, linhas);
        fscanf(file, "%i", variacaoCor);
        
        *quantidadePixel = (*colunas) * (*linhas);
        *vetorDePixel = malloc(*quantidadePixel * sizeof(Pixel));
        for (int i = 0; i < *quantidadePixel; i++)
        {
                fscanf(file, "%i", &(*vetorDePixel)[i].r);
                fscanf(file, "%i", &(*vetorDePixel)[i].g);
                fscanf(file, "%i", &(*vetorDePixel)[i].b);
        }
        
        fclose(file);
    
}

void salvarImagem(char nomeCompletoNovo[],int colunas,int linhas,Pixel *vetorDePixel,int variacaoCor, int quantidadePixel){
    
    FILE *file = fopen(nomeCompletoNovo, "w");
    quantidadePixel = (colunas) * (linhas);
    fprintf(file, "P3\n");
    fprintf(file, "%d %d\n", colunas, linhas);
    fprintf(file, "%d\n", variacaoCor);

     for (int i = 0; i < quantidadePixel; i++)
        {
            fprintf(file,"%d\n", (vetorDePixel)[i].r);
            fprintf(file,"%d\n", (vetorDePixel)[i].g);
            fprintf(file,"%d\n", (vetorDePixel)[i].b);

        }
    
    fclose(file);
    free(vetorDePixel);

}

void escalaCinza(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor){
    FILE *file = fopen(nomeCompletoNovo, "w");
    quantidadePixel = (colunas) * (linhas);
    fprintf(file, "P3\n");
    fprintf(file, "%d %d\n", colunas, linhas);
    fprintf(file, "%d\n", variacaoCor);
    for (int i = 0; i < quantidadePixel; i++)
        {
            fprintf(file,"%d\n", (vetorDePixel)[i].r = (vetorDePixel)[i].r * 0.3) + ((vetorDePixel)[i].g *0.59) + ((vetorDePixel)[i].b * 0.11);
            fprintf(file,"%d\n", (vetorDePixel)[i].g = (vetorDePixel)[i].r);
            fprintf(file,"%d\n", (vetorDePixel)[i].b = (vetorDePixel)[i].r);

        }
    
    fclose(file);
    free(vetorDePixel);

}

void detBorda(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor){
    FILE *file = fopen(nomeCompletoNovo, "w");
    quantidadePixel = (colunas) * (linhas);
    fprintf(file, "P3\n");
    fprintf(file, "%d %d\n", colunas, linhas);
    fprintf(file, "%d\n", variacaoCor);

    for (int i = 0; i < quantidadePixel; i++)
        {
            (vetorDePixel)[i].r = ((vetorDePixel)[i].r * -1) + ((vetorDePixel)[i + 1].r * -1) + ((vetorDePixel)[i + 2].r * -1) + ((vetorDePixel)[i + 3].r * -1) + ((vetorDePixel)[i + 4].r * 8) + ((vetorDePixel)[i + 5].r * -1) + ((vetorDePixel)[i + 6].r * -1) + ((vetorDePixel)[i + 7].r * -1) + ((vetorDePixel)[i + 8].r * -1);
            (vetorDePixel)[i].g = ((vetorDePixel)[i].g * -1) + ((vetorDePixel)[i + 1].g * -1) + ((vetorDePixel)[i + 2].g * -1) + ((vetorDePixel)[i + 3].g * -1) + ((vetorDePixel)[i + 4].g * 8) + ((vetorDePixel)[i + 5].g * -1) + ((vetorDePixel)[i + 6].g * -1) + ((vetorDePixel)[i + 7].g * -1) + ((vetorDePixel)[i + 8].g * -1);
            (vetorDePixel)[i].b = ((vetorDePixel)[i].b * -1) + ((vetorDePixel)[i + 1].b * -1) + ((vetorDePixel)[i + 2].b * -1) + ((vetorDePixel)[i + 3].b * -1) + ((vetorDePixel)[i + 4].b * 8) + ((vetorDePixel)[i + 5].b * -1) + ((vetorDePixel)[i + 6].b * -1) + ((vetorDePixel)[i + 7].b * -1) + ((vetorDePixel)[i + 8].b * -1);
            if ((vetorDePixel)[i].r > 255)
            {
                (vetorDePixel)[i].r = 255;
            }else if ((vetorDePixel)[i].r < 0)
            {
                (vetorDePixel)[i].r = 0;
            }
            if ((vetorDePixel)[i].g > 255)
            {
                (vetorDePixel)[i].g = 255;
            }else if ((vetorDePixel)[i].g < 0)
            {
                (vetorDePixel)[i].g = 0;
            }
            if ((vetorDePixel)[i].b > 255)
            {
                (vetorDePixel)[i].b = 255;
            }else if ((vetorDePixel)[i].b < 0)
            {
                (vetorDePixel)[i].b = 0;
            }           

            fprintf(file,"%d\n", (vetorDePixel)[i].r);
            fprintf(file,"%d\n", (vetorDePixel)[i].g);
            fprintf(file,"%d\n", (vetorDePixel)[i].b);
            
        }
    
    fclose(file);
    free(vetorDePixel);

}

void borrarImagem(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor){
    FILE *file = fopen(nomeCompletoNovo, "w");
    quantidadePixel = (colunas) * (linhas);
    fprintf(file, "P3\n");
    fprintf(file, "%d %d\n", colunas, linhas);
    fprintf(file, "%d\n", variacaoCor);

    for (int i = 0; i < quantidadePixel; i++)
        {
            (vetorDePixel)[i].r = (((vetorDePixel)[i].r) + ((vetorDePixel)[i + 1].r) + ((vetorDePixel)[i + 2].r) + ((vetorDePixel)[i + 3].r) + ((vetorDePixel)[i + 4].r) + ((vetorDePixel)[i + 5].r) + ((vetorDePixel)[i + 6].r) + ((vetorDePixel)[i + 7].r) + ((vetorDePixel)[i + 8].r))/9;
            (vetorDePixel)[i].g = (((vetorDePixel)[i].g) + ((vetorDePixel)[i + 1].g) + ((vetorDePixel)[i + 2].g) + ((vetorDePixel)[i + 3].g) + ((vetorDePixel)[i + 4].g) + ((vetorDePixel)[i + 5].g) + ((vetorDePixel)[i + 6].g) + ((vetorDePixel)[i + 7].g) + ((vetorDePixel)[i + 8].g))/9;
            (vetorDePixel)[i].b = (((vetorDePixel)[i].b) + ((vetorDePixel)[i + 1].b) + ((vetorDePixel)[i + 2].b) + ((vetorDePixel)[i + 3].b) + ((vetorDePixel)[i + 4].b) + ((vetorDePixel)[i + 5].b) + ((vetorDePixel)[i + 6].b) + ((vetorDePixel)[i + 7].b) + ((vetorDePixel)[i + 8].b))/9;
            if ((vetorDePixel)[i].r > 255)
            {
                (vetorDePixel)[i].r = 255;
            }else if ((vetorDePixel)[i].r < 0)
            {
                (vetorDePixel)[i].r = 0;
            }
            if ((vetorDePixel)[i].g > 255)
            {
                (vetorDePixel)[i].g = 255;
            }else if ((vetorDePixel)[i].g < 0)
            {
                (vetorDePixel)[i].g = 0;
            }
            if ((vetorDePixel)[i].b > 255)
            {
                (vetorDePixel)[i].b = 255;
            }else if ((vetorDePixel)[i].b < 0)
            {
                (vetorDePixel)[i].b = 0;
            }           

            fprintf(file,"%d\n", (vetorDePixel)[i].r);
            fprintf(file,"%d\n", (vetorDePixel)[i].g);
            fprintf(file,"%d\n", (vetorDePixel)[i].b);
            
        }
    
    fclose(file);
    free(vetorDePixel);
}

void sharpenImagem(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor){
    FILE *file = fopen(nomeCompletoNovo, "w");
    quantidadePixel = (colunas) * (linhas);
    fprintf(file, "P3\n");
    fprintf(file, "%d %d\n", colunas, linhas);
    fprintf(file, "%d\n", variacaoCor);

    for (int i = 0; i < quantidadePixel; i++)
        {
            (vetorDePixel)[i].r = ((vetorDePixel)[i].r * 0) + ((vetorDePixel)[i + 1].r * -1) + ((vetorDePixel)[i + 2].r * 0) + ((vetorDePixel)[i + 3].r * -1) + ((vetorDePixel)[i + 4].r * 5) + ((vetorDePixel)[i + 5].r * -1) + ((vetorDePixel)[i + 6].r * 0) + ((vetorDePixel)[i + 7].r * -1) + ((vetorDePixel)[i + 8].r * 0);
            (vetorDePixel)[i].g = ((vetorDePixel)[i].g * 0) + ((vetorDePixel)[i + 1].g * -1) + ((vetorDePixel)[i + 2].g * 0) + ((vetorDePixel)[i + 3].g * -1) + ((vetorDePixel)[i + 4].g * 5) + ((vetorDePixel)[i + 5].g * -1) + ((vetorDePixel)[i + 6].g * 0) + ((vetorDePixel)[i + 7].g * -1) + ((vetorDePixel)[i + 8].g * 0);
            (vetorDePixel)[i].b = ((vetorDePixel)[i].b * 0) + ((vetorDePixel)[i + 1].b * -1) + ((vetorDePixel)[i + 2].b * 0) + ((vetorDePixel)[i + 3].b * -1) + ((vetorDePixel)[i + 4].b * 5) + ((vetorDePixel)[i + 5].b * -1) + ((vetorDePixel)[i + 6].b * 0) + ((vetorDePixel)[i + 7].b * -1) + ((vetorDePixel)[i + 8].b * 0);
            if ((vetorDePixel)[i].r > 255)
            {
                (vetorDePixel)[i].r = 255;
            }else if ((vetorDePixel)[i].r < 0)
            {
                (vetorDePixel)[i].r = 0;
            }
            if ((vetorDePixel)[i].g > 255)
            {
                (vetorDePixel)[i].g = 255;
            }else if ((vetorDePixel)[i].g < 0)
            {
                (vetorDePixel)[i].g = 0;
            }
            if ((vetorDePixel)[i].b > 255)
            {
                (vetorDePixel)[i].b = 255;
            }else if ((vetorDePixel)[i].b < 0)
            {
                (vetorDePixel)[i].b = 0;
            }           

            fprintf(file,"%d\n", (vetorDePixel)[i].r);
            fprintf(file,"%d\n", (vetorDePixel)[i].g);
            fprintf(file,"%d\n", (vetorDePixel)[i].b);
            
        }
    
    fclose(file);
    free(vetorDePixel);
}