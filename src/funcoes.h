#ifndef FUNCOES_H
#define FUNCOES_H
#include <stdio.h>
#include <stdlib.h>
//#include "funcoes.c"

void caminhoImagem(char nomeImagem[], char nomeCompleto[],char nomeImagemNova[],char nomeCompletoNovo[]);

void lerImagem(char nomeCompleto[],int *colunas, int *linhas, Pixel **vetorDePixel, int *variacaoCor, int *quantidadePixel, char formato[]);

void salvarImagem(char nomeCompletoNovo[],int colunas,int linhas,Pixel *vetorDePixel,int variacaoCor, int quantidadePixel);

void escalaCinza(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor);

void detBorda(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor);

void borrarImagem(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor);

void sharpenImagem(char nomeCompletoNovo[], int linhas, int colunas, int quantidadePixel, Pixel *vetorDePixel, int variacaoCor);

#endif