#ifndef TIPOS
#define TIPOS

typedef struct Pixels
{
    int r, g, b;
}Pixel;


typedef struct Imagens
{
    char *formato;
    int altura, largura;
    int varCor;
    Pixel *pixels;
    int qtdpixel;
}Imagem;


#endif