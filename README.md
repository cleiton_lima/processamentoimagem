PROJETO FINAL DA DISCIPLINA INTRODUÇÃO AS TÉCNICAS DE PROGRAMAÇÃO

- INTEGRANTES DO GRUPO: Antonio Cleiton Ribeiro de Lima e Moises de Freitas Queiroz

- PROCESSAMENTO DE IMAGENS NO FORMATO PPM

Para a construção do projeto foram requeridos vários conhecimentos passados ao longo da disciplina em relação aos critérios de programação, tais como:
- Uso de arranhos/matrizez -> Atendido
- Uso de resgistros(struct) -> Atendido
- Definição de novos tipos de dados através de typedef -> Atendido
- Uso de alocação dinâmica -> Atendido
- Pelo menos uma função/procedimento que usa recursão -> Não atendido
- Leitura de arquivos -> Atendido
- Modularização do programa em diferentes arquivos (uso de diferentes arquivos
.c e .h, cada um com sua funcionalidade) -> Atendido
- Definição de um padrão de indentação do código fonte e de nomenclatura das
sub-rotinas e variáveis, e a adequação a este padrão -> Atendido
- Documentação adequada do código-fonte -> Atendido

- O QUE FOI FEITO
- Foram feitas as funiconalidades básicas requeridas para o projeto listadas a seguir:
- Ler imagem;
- Salvar imagem;
- Escala de cinza;
- Detecção de bordas;
- Filtro de Blurring;
- Filtro de Sharpering;


- O QUE NÃO FOI FEITO
- Ampliar, reduzir e rotacionar a imagem;
- Funcionalidade extra;

- O QUE SERIA FEITO DIFERENTE?
- Ao invés de tratar as imagens com vetor, tratar com matriz para facilitar algumas funcionalidades como ampliar, reduzir e rotacionar.

- COMO COMPILAR O PROJETO
- Para rodar o projeto, cria-se uma biblioteca estática 
- Com o terminal aberto no diretório em que se encontra projeto, execute os seguintes comandos:
- Primeiro, gere os arquivos objeto(.o), binários de cada .c
$ gcc -c src/funcoes.c -o funcoes.o

- Use o programa ar para juntar os .o em um arquivo .a
$ ar rcs libfuncoes.a src/funcoes.o

- Gere o arquivo objeto(.o) do arquivo principal .c
$ gcc processamento.c -o processamento.o

- Com o arquivo .a(biblioteca estática), pode-se gerar o executável
$ gcc processamento.o -lfuncoes -o exec

- Para rodar o programa, execute a seguinte linha de comando:
$ ./exec





